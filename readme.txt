=== Plugin Name ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: http://www.ecoindex.fr/quest-ce-que-ecoindex/
Tags: ecoindex, performance
Requires at least: 4.7
Tested up to: 5.4
Stable tag: 4.3
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

EcoIndex.fr est un service en ligne conçu et proposé par Frédéric Bordage (GreenIT.fr) et réalisé par Gwenael Chailleu et Thomas Samain de la société Nxtweb. De nombreuses organisations ont contribué à cette deuxième version. Voir la liste ci-dessous.

L’algorithme EcoIndex, le service EcoIndex.fr et les autres outils associés, ainsi que les résultats fournis par l’algorithme sont proposés sous licence Creative Commons CC-By-NC-ND.

Cela signifie que tout le monde peut utiliser l’algorithme, les outils et les résultats, à condition de citer la source des chiffres obtenus : “EcoIndex.fr” et un lien vers le service (http://ecoindex.fr). Il n’est pas possible d’en faire un usage commercial.

Ce service évoluera en fonction de vos envies / besoins et de vos contributions.

== Description ==

EcoIndex est un outil communautaire, gratuit et transparent qui, pour une URL donnée, permet d’évaluer :
- sa performance environnementale absolue à l’aide d’un score sur 100 (higher is better) ;
- sa performance environnementale relative à l’aide d’une note de A à G ;
- l’empreinte technique de la page (poids, complexité, etc.) ;
- l’empreinte environnementale associée (gaz à effet de serre et eau).

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 1.0 =

== Upgrade Notice ==

= 1.1 =
Ajout des fichiers du dépôt directement dans le dossier du plugin.

= 1.2 =
Ajout d'une page d'administration dans "Outils"->EcoIndex pour choisir quand afficher l'indicateur.