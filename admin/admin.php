<?php 
/*
 * Add menu to the Admin Control Panel
 */

add_action( 'admin_menu', 'ecoindex_admin' );

function ecoindex_admin() {

    add_submenu_page(
        'tools.php',
        'ihag_ecoindex_admin',
        'EcoIndex',
        'manage_options',
        'ihag_ecoindex_admin',
        'ihag_ecoindex_admin',
    );

}

function ihag_ecoindex_admin(){
$checked = 'checked="checked"';
?>
    <div class="wrap">

        <h1><?php _e('Affichage d\'EcoIndex :', 'ecoindex'); ?></h1>

        <form action="" method="post" class="">

            <p>
                <input type="radio" id="only_admin" name="who_can_see" value="0" <?php if(get_option('Choice') == 0) { echo $checked; }; ?>>
                <label for="only_admin"><?php _e('Seulement visible sur le site pour l\'administrateur lorsqu\'il est connecté.', 'ecoindex');?></label>
            </p>    

            <p>
                <input type="radio" id="all_site" name="who_can_see" value="1" <?php if(get_option('Choice') == 1) { echo $checked; };?>>
                <label for="all_site"><?php _e('Toujours visible sur le site pour tous les utilisateurs.', 'ecoindex');?></label>
            </p>
            
            <input type="hidden" name="record_choice_ecoindex">
            <input type="submit" class="button" value="<?php _e('Enregistrer', 'ecoindex') ?>">
    
        </form>

    <?php

}


add_action( 'init', function(){
    if(isset($_POST['who_can_see'])){
        $option = 'choice_ecoindex';
        $value = $_POST['who_can_see'];
        update_option($option, $value);
    }
} );
