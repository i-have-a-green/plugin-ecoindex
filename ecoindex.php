<?php
/**
* Plugin Name: EcoIndex
* Plugin URI: http://www.ecoindex.fr/quest-ce-que-ecoindex/
* Description: Plugin EcoIndex.
* Version: 1.2
* Author: I Have a Green
* Author URI: https://ihaveagreen.fr/
**/

include_once('admin/admin.php');


add_action( 'wp_enqueue_scripts', 'ihag_ecoindex', 998 );
function ihag_ecoindex(){
    $current_user = wp_get_current_user();
    

    $choice = get_option('choice_ecoindex');
    $id_post = get_the_id();
 
    $display_on_front = false;
    // if admin don't change parameter by default of if display only for admin 
    if($choice == 0 && user_can( $current_user, 'administrator')){
        $display_on_front = true;
    } 
    // if display everytime
    else if ($choice == 1){
        $display_on_front = true;
    }

    //if display_on_front = true
      
    //if already record, display it 
    if(get_post_meta($id_post, 'ecoindex')):
        if($display_on_front):
            global $post;
            $post->post_content = $post->post_content.get_post_meta($id_post, 'ecoindex', true);
        endif;
    //new on, record and diplay it with ecoindex.js
    else:
        //$src = plugin_dir_url( __FILE__ ).'./ecoindex-light-js/ecoindex.js' ;
        $src = 'https://cdn.jsdelivr.net/gh/simonvdfr/ecoindex-light-js@main/ecoindex.min.js';
        wp_enqueue_script('ihag_ecoindex', $src, array(), false, true);
        wp_localize_script('ihag_ecoindex', 'ihag_id_post', (string)$id_post);
        wp_localize_script('ihag_ecoindex', 'ihag_rest', get_bloginfo('url') . '/wp-json/ihag_ecoindex/');
        
        //Si on veut afficher que pour admin : 1ere publication on ne veut pas afficher ecoindex 
        wp_localize_script('ihag_ecoindex', 'display_ecoindex', (string)$display_on_front);
       
        
    endif;
    
}

// -----
add_action('rest_api_init', function() {
    register_rest_route( 'ihag_ecoindex', 'insert_ecoindex',
        array(
            'methods' 				=> 'POST', //WP_REST_Server::READABLE,
            'callback'        		=> 'insert_ecoindex',
            'permission_callback' 	=> array(),
            'args' 					=> array(),
        )
    );
});

function insert_ecoindex(){
    update_post_meta( $_POST['ihag_id_post'], 'ecoindex', $_POST['HTML_EcoIndexGrade'] );
}

// ----- When post save -> delete ecoindex recorded
add_action( 'save_post', 'delete_post_meta_ecoindex', 10,3 );
function delete_post_meta_ecoindex( $post_id, $post, $update ) {
    delete_post_meta($post_id, 'ecoindex');
}

// -----
